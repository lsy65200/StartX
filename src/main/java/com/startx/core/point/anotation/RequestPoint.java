package com.startx.core.point.anotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 方法注解
 */
@Target(value={ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface RequestPoint {
	String[] value();
	RequestMethod[] method() default {RequestMethod.GET};
	ResponseType type() default ResponseType.JSON;
}
