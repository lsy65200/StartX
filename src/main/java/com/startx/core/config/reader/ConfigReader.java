package com.startx.core.config.reader;

import java.io.IOException;

public interface ConfigReader {
	/**
	 * 读取配置
	 * @throws Exception 
	 */
	public void read() throws IOException, Exception;
}
